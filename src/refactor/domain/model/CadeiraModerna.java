package refactor.domain.model;

import refactor.interfaces.Cadeira;

public class CadeiraModerna implements Cadeira {

    @Override
    public void imprimir() {
        System.out.println("cadeira moderna");
    }
    
}
