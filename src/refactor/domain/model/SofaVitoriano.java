package refactor.domain.model;

import refactor.interfaces.Sofa;

public class SofaVitoriano implements Sofa{

    @Override
    public void imprimir() {
        System.out.println("Sofa vitoriana");
    }
    
}
