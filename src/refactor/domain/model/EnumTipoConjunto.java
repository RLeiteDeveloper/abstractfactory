package refactor.domain.model;

import java.util.Random;

public enum EnumTipoConjunto {
  
    VITORIANO(1, "Vitoriano"), MODERNO(2,"Moderno");

    private final int valor;
    private final String descricao;

    EnumTipoConjunto(int valor, String descricao){
      this.valor = valor;
      this.descricao  = descricao;
    }

    public int getValor(){
        return valor;
    }

    public String getDescricao(){
      return descricao;
  }

    public static EnumTipoConjunto gerarValorRandomico() {
      var values = EnumTipoConjunto.values();
      var length = values.length;
      var randIndex = new Random().nextInt(length);
      return values[randIndex];
  }
}
