package refactor.domain.model;

import refactor.interfaces.Sofa;

public class SofaModerno implements Sofa {

    @Override
    public void imprimir() {
        System.out.println("Sofa moderno");
    }
    
}
