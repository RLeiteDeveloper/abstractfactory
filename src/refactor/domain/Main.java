package refactor.domain;

import java.lang.reflect.InvocationTargetException;

import refactor.domain.factory.ModernoFactory;
import refactor.domain.factory.VitorianoFactory;
import refactor.domain.model.EnumTipoConjunto;
import refactor.interfaces.AbsFactory;

public class Main {
    
    private static Application application;
    private static AbsFactory absFactory;
    
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException {
        application = new Application();
        escolherConjunto(EnumTipoConjunto.VITORIANO);
    }

    public static void escolherConjunto(EnumTipoConjunto enumTipoConjunto) throws ClassNotFoundException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, InstantiationException{
        
        if(enumTipoConjunto == EnumTipoConjunto.VITORIANO){
            absFactory = new VitorianoFactory();
        }else if(enumTipoConjunto == EnumTipoConjunto.MODERNO){
            absFactory = new ModernoFactory();
        }
       
        application.montarConjunto(absFactory);
        application.mostrarConjunto();

    }

}
