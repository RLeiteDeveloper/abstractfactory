package refactor.domain.factory;

import refactor.domain.model.CadeiraModerna;
import refactor.domain.model.SofaModerno;
import refactor.interfaces.AbsFactory;
import refactor.interfaces.Cadeira;
import refactor.interfaces.Sofa;

public class ModernoFactory implements AbsFactory {

    @Override
    public Cadeira criarCadeira() {
        return new CadeiraModerna();
    }

    @Override
    public Sofa criarSofa() {
        return new SofaModerno();
    }
  
}
