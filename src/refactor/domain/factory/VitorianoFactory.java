package refactor.domain.factory;

import refactor.domain.model.CadeiraVitoriana;
import refactor.domain.model.SofaVitoriano;
import refactor.interfaces.AbsFactory;
import refactor.interfaces.Cadeira;
import refactor.interfaces.Sofa;

public class VitorianoFactory implements AbsFactory {

    @Override
    public Cadeira criarCadeira() {
        return new CadeiraVitoriana();
    }

    @Override
    public Sofa criarSofa() {
        return new SofaVitoriano();
    }
    
}
