package refactor.domain;

import refactor.interfaces.AbsFactory;
import refactor.interfaces.Cadeira;
import refactor.interfaces.Sofa;

public class Application {
    
    private Cadeira cadeira;
    private Sofa sofa;

    public void montarConjunto(AbsFactory absFactory){
        this.cadeira = absFactory.criarCadeira();
        this.sofa = absFactory.criarSofa();
    }

    public void mostrarConjunto(){
        cadeira.imprimir();
        sofa.imprimir();
    }



}
