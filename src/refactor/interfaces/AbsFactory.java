package refactor.interfaces;

public interface AbsFactory {
    Cadeira criarCadeira();
    Sofa criarSofa();
}
